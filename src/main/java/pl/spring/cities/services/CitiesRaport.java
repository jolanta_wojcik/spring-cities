package pl.spring.cities.services;

import java.util.List;

public interface CitiesRaport {

	List<String> cities();
	String country();
}
