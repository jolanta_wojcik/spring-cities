package pl.spring.cities.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class PolishCities implements CitiesRaport{
	
	public List<String> cities() {
		List<String> plCities = new ArrayList<String>();
		plCities.add("Krakow");
		plCities.add("Warszawa");
		plCities.add("Wroclaw");
		return plCities;
	}

	public String country() {
		return "Poland";
	}
}
